<?php

/**
 * Fired during plugin activation
 *
 * @since      1.0.0
 *
 * @package    Password_Hero_Generator
 * @subpackage Password_Hero_Generator/includes
 */

/**
 * Fired during plugin activation.
 *
 *
 * @since      1.0.0
 * @package    Password_Hero_Generator
 * @subpackage Password_Hero_Generator/includes
 * @author     Sumit Sehgal <sumit@cybertrontechnologies.com>
 */
class Password_Hero_Generator_Activator {

	/**
	 *
	 *
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

		global $wpdb;

		$table_name = $wpdb->prefix . 'password_hero_rules';
		$charset_collate = $wpdb->get_charset_collate();

		$sql = "CREATE TABLE IF NOT EXISTS $table_name (
			id bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
			name varchar(50) NOT NULL UNIQUE KEY, rule text NULL, order_no decimal(3) default 0, platform_icon_class varchar(25) NULL, fontawesome_class varchar(25) NULL, status enum('Active', 'Inactive') default 'Active' ) $charset_collate;";

		require_once( ABSPATH . '/wp-admin/includes/upgrade.php' );
		dbDelta($sql);

		$table_name = $wpdb->prefix . 'password_hero_platform_rule_titles';
		$sql = "CREATE TABLE IF NOT EXISTS $table_name (
			id bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
			rule_id bigint(20), lbl_title varchar(255) NOT NULL, rule_check enum('length', 'uppercase', 'lowercase', 'number', 'symbol', 'other') default 'other') $charset_collate;";
			
		dbDelta($sql);

		$table = $wpdb->prefix . 'password_hero_settings';

		$sql = "CREATE TABLE IF NOT EXISTS $table (
			id bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
			minimum_slider int(3) default 1,
			maximum_slider  int(3) default 25,
			default_slider	int(3) default 9,
			uppercase_visible tinyint(1) default 1,
			lowercase_visible tinyint(1) default 1,
			number_visible tinyint(1) default 1,
			symbol_visible tinyint(1) default 1,
			platform_visible tinyint(1) default 1,
			copy_title text null,
			copy_description text null,
			copy_image varchar(255) null,
			copy_button_text varchar(255),
			copy_button_link varchar(255),
			send_title text null,
			send_description text null,
			send_image varchar(255) null,
			send_button varchar(255) null,
			send_button_link varchar(255) null,
			is_opt tinyint(1) default 1,
			opt_message text null,
			password_hero_redirect text null,
			password_hero_redirect_type enum('same-window', 'new-window') default 'same-window'

			)
			$charset_collate;";

		dbDelta($sql);

		$wpdb->get_results("SELECT * FROM ".$table);
		if($wpdb->num_rows <= 0){ 
			$sql = "INSERT INTO ".$table."(copy_title, copy_description, copy_button_text, copy_button_link, send_title, send_description, send_button, send_button_link) VALUES('Manage your accounts with ease', 'Description', 'Try free for 14 days', '#', 'The world\'s most-loved password manager', 'Description', 'Try 1Password Free', '#')";
			$wpdb->query($sql);
			// dbDelta($sql);
		}



		$table = $wpdb->prefix . 'password_hero_copy_settings';

		$sql = "CREATE TABLE IF NOT EXISTS $table (
			id bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
			title text null,
			highlight_title text null,
			tagline text null,
			subtext text null,
			agreement_text text null

			)
			$charset_collate;";

		dbDelta($sql);


		$wpdb->get_results("SELECT * FROM ".$table);
		if($wpdb->num_rows <= 0){ 
			$sql = "INSERT INTO ".$table."(title, highlight_title, tagline, subtext, agreement_text) VALUES(' Password Copied!', 'Todays Security Statistic', '4 out of 10 people have had their personal data compromised online. 47% have lost money as a result. (Google)', 'Secure &amp; manage your online accounts FREE with 1Password!', 'By submitting this form I have read and accept the privacy policy')";
			$wpdb->query($sql);
			// dbDelta($sql);
		}

		
		

	}

}