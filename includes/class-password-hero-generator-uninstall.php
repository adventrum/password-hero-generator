<?php

/**
 * Fired during plugin uninstall
 *
 * @link       http://
 * @since      1.0.0
 *
 * @package    Password_Hero_Generator
 * @subpackage Password_Hero_Generator/includes
 */

/**
 * Fired during plugin activation.
 *
 *
 * @since      1.0.0
 * @package    Password_Hero_Generator
 * @subpackage Password_Hero_Generator/includes
 * @author     Sumit Sehgal <sumit@cybertrontechnologies.com>
 */
class Password_Hero_Generator_Uninstall {

	/**
	 *
	 *
	 *
	 * @since    1.0.0
	 */
	public static function uninstall() {

		global $wpdb;

		$table_name = $wpdb->prefix . 'password_hero_rules';

		$sql = "DROP TABLE IF EXISTS ".$table_name;

		$wpdb->query($sql);

		// Other Table
		$table_name = $wpdb->prefix . 'password_hero_platform_rule_titles';

		$sql = "DROP TABLE IF EXISTS ".$table_name;

		$wpdb->query($sql);

		$table = $wpdb->prefix . 'password_hero_settings';

		$sql = "DROP TABLE IF EXISTS ".$table;

		$wpdb->query($sql);

	}

}