<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://
 * @since      1.0.0
 *
 * @package    Password_Hero_Generator
 * @subpackage Password_Hero_Generator/includes
 */

/**
 * Fired during plugin activation.
 *
 *
 * @since      1.0.0
 * @package    Password_Hero_Generator
 * @subpackage Password_Hero_Generator/includes
 * @author     Sumit Sehgal <sumit@cybertrontechnologies.com>
 */
class Password_Hero_Generator_Deactivator {

	/**
	 *
	 *
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}