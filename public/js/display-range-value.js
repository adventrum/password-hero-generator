(function($) {
    // display value of range while sliding
    $("input.display-value").change(function() {
        $(this).next().val($(this).val());
    })

    $(document).ready(function() {
        slider_styleadded();
    });
    // see: http://stackoverflow.com/a/18389801/1148249
    $('input[type="range"]').change(function() {
        slider_styleadded();
    });

    function slider_styleadded() {
        var init = $('input[type="range"]');
        var val = ($(init).val() - $(init).attr('min')) / ($(init).attr('max') - $(init).attr('min'));
        $(init).css('background-image',
            '-webkit-gradient(linear, left top, right top, ' +
            'color-stop(' + val + ', #24A7FF), ' +
            'color-stop(' + val + ', #C5C5C5)' +
            ')'
        );
    }


}(jQuery));