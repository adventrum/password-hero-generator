<button type="button" style="display: none;" id="email-complete">Email Complete</button>
<section class="hero copied">
    <div class="container">
        <div class="hero-text">
            <div class="head">
                <h1><?php echo $attributes["title"]; ?></h1>
                <p><?php echo $attributes["subtitle"]; ?></p>
            </div>
            <div class="copied-box">
                <div class="copied-head">
                    <?php if(isset($copiedSetting) && !empty($copiedSetting->title) ) { ?>
                        <h2> <img src="<?php echo esc_url(plugins_url('images/check-icon.png', dirname(__FILE__))); ?>" alt="icon" /> <?php echo $copiedSetting->title; ?></h2>
                    <?php 
                    }
                    ?>
                </div>
                <div class="security-statistic">
                    <?php if(isset($copiedSetting) && !empty($copiedSetting->highlight_title) ) { ?>
                        <h3><?php echo $copiedSetting->highlight_title; ?></h3>
                    <?php } ?>
                    <?php if(isset($copiedSetting) && !empty($copiedSetting->tagline) ) { ?>
                        <p><?php echo $copiedSetting->tagline; ?></p>
                    <?php } ?>
                </div>


                <div class="one-pass-cta">
                    <h4>
                        <?php if(isset($copiedSetting) && !empty($copiedSetting->subtext) ) { ?>
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo-dark-1.png" alt="icon" />
                        <span><?php echo $copiedSetting->subtext; ?></span>
                        <?php } ?>
                    </h4>
                </div>


                <div class="email-sec">
                    <p>Send password to your email</p>
                    <form method="post" id="email-frm">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="josh@passdowdhero.com" name="p_emails">
                        </div>
                        <div class="send-btn">
                            <button class="send-button " type="submit" name="email_send" data-toggle="modal" data-target="#send">
                                <i class="far fa-paper-plane"></i> <span>Send</span>
                            </button>
                        </div>
                        <div class="email-opt">
                            <input type="checkbox" id="opt" class="form-control" name="opt_in"> 
                            <?php if(isset($copiedSetting) && !empty($copiedSetting->agreement_text) ) { ?>
                            <label for="opt"><?php echo $copiedSetting->agreement_text; ?></label>
                            <?php } ?>

                        </div>
                    </form>
                    <div class="alert alert-success" role="alert" id="email-send-message" style="display: none;">
                        Email Successfully Sent!
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>