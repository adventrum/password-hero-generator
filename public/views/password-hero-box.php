<?php
    $affiliatedUrl = (isset($attributes['affiliated_url']) && !empty($attributes['affiliated_url'])) ? $attributes['affiliated_url'] : $setting->copy_button_link;

    $redirectUrl = (isset($setting) && !empty($setting->password_hero_redirect)) ? $setting->password_hero_redirect : "";

?>
<form style="display: none;" <?php if(isset($setting) && $setting->password_hero_redirect_type == "new-window"){ ?>  target="_blank" <?php } ?>  action="<?php echo $redirectUrl; ?>" base-action="<?php echo $redirectUrl; ?>" id="frm-copied"></form>
<input type="hidden" id="sid" pageid="<?php echo get_the_ID(); ?>" region="<?php echo getenv('HTTP_GEOIP_COUNTRY_CODE'); ?>" />


<section class="hero">
    <div class="hero-text">
        <div class="hero-box">
            <ul class="nav nav-tabs" id="major-links" role="tablist">
                <li class="nav-item" role="presentation">
                    <a class="nav-link active ga-password-tab" id="home-tab" data-toggle="tab" href="#password" role="tab" aria-controls="password" aria-selected="true" ptype="password" data-text="Password">Password</a>
                </li>
                <li class="nav-item" role="presentation">
                    <a class="nav-link ga-password-tab" id="profile-tab" data-toggle="tab" href="#passphrasea" role="tab" aria-controls="passphrase" aria-selected="false" ptype="passphrase" data-text="Passphrase">Passphrase</a>
                </li>
                <li class="nav-item" role="presentation">
                    <a class="nav-link ga-password-tab" id="contact-tab" data-toggle="tab" href="#pina" role="tab" aria-controls="pin" aria-selected="false" ptype="pin" data-text="Pin">PIN</a>
                </li>
            </ul>
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane show active" id="password" role="tabpanel" aria-labelledby="home-tab">
                    <div class="password-box">
                        <div class="generated-password" data-module="passwordGenerator">
                            <div class="generated-password-box">
                                <form id="generated-password-form" class="blinking">
                                    <div class="form-group">
                                        <input value="" id="generated-password" spellcheck="false" class="password-input" type="text" data-character-set="a-z,A-Z,0-9,#" rel="gp">
                                    </div>
                                </form>
                                <div class="generated-wrapper">
                                    <div class="week-pass" id="strength-badge">
                                        <p style="background-color:rgba(0, 255, 0, 0.1);"><a href="javascript:void(0);" class="to-strong" style="color:green;">Strong</a></p>
                                    </div>
                                    <div class="up-arrow to-strong" style="display: none;">
                                        <i class="fas fa-angle-up"></i>
                                    </div>
                                    <button class="password-icon password-icon-generate to-strong d-none d-md-block ga-reset-password" style="background-image: url(<?php echo esc_url(plugins_url('images/icon-generate.png', dirname(__FILE__))); ?>);" >
                                        <span class="tooltip-content ga-reset-password">
                                            Generate
                                        </span>
                                    </button>
                                    <button class="password-icon password-icon-copy copyPass d-none d-md-block ga-copy-pass affiliate-link" data-copy="copyPassword"  target-url="<?php echo $affiliatedUrl; ?>">
                                        <span class="tooltip-content ga-copy-pass" data-copy="copyPassword">
                                            <img src="<?php echo esc_url(plugins_url('images/copy-icon.png', dirname(__FILE__))); ?>" alt="icon" class="ga-copy-pass" data-copy="copyPassword" /> Copy
                                        </span>
                                    </button>
                                </div>
                                <div class="strength-wrapper">
                                    <div class="generated-password-strength" style="width: 100%;background-color:green;"></div>
                                </div>
                            </div>

                        </div>

                        <div class="form-comp row">
                            <div class="col-md-5">
                                <?php if (isset($setting->platform_visible) && $setting->platform_visible == 1) : ?>
                                    <div class="platform-comp">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="Platform">
                                                    <p>Platform compatibility <img style="display:none;" src="<?php echo get_template_directory_uri(); ?>/assets/images/question.png" alt="icon" /></p>
                                                    <ul class="platform-list">
                                                        <?php if (!empty($overallRules)) : ?>
                                                            <?php foreach ($overallRules as $rule) : ?>
                                                                <li>
                                                                    <a class="<?php echo $rule['platform_icon_class']; ?> platform-pattern" href="javascript:void(0)" pattern="<?php echo $rule['rule']; ?>"><i class="fab <?php echo $rule['fontawesome_class']; ?>"></i></a>
                                                                    <div class="exclamation-info"><i class="fas fa-exclamation"></i></div>
                                                                    <?php if (isset($rule['titles']) && !empty($rule['titles'])) : ?>
                                                                        <div class="tooltip-box">
                                                                            <ul>
                                                                                <?php foreach ($rule['titles'] as $title) : ?>
                                                                                    <li type="<?php echo $title['rule_check']; ?>"><span><?php echo $title['lbl_title']; ?></span> <i class="fas fa-times-circle"></i></li>
                                                                                <?php endforeach; ?>
                                                                            </ul>
                                                                        </div>
                                                                    <?php endif; ?>
                                                                </li>
                                                            <?php endforeach; ?>
                                                        <?php endif; ?>
                                                    </ul>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                <?php endif; ?>
                            </div>
                            <div class="col-md-7">
                                <form id="pg-form" class="pg-settings">


                                    <div class="checkbox-row">
                                        <div class="checkbox-wrap">
                                            <?php if (isset($setting->uppercase_visible) && $setting->uppercase_visible == 1) : ?>
                                                <div class="checkbox">
                                                    <input class="checkbox-input password-filter" id="pg-uppercase" type="checkbox" name="uppercase" checked chset="A-Z">
                                                    <label class="checkbox-label" for="pg-uppercase">
                                                        Uppercase
                                                    </label>
                                                </div>
                                            <?php endif; ?>

                                            <?php if (isset($setting->lowercase_visible) && $setting->lowercase_visible == 1) : ?>
                                                <div class="checkbox">
                                                    <input class="checkbox-input password-filter" id="pg-lowercase" type="checkbox" name="lowercase" checked chset="a-z">
                                                    <label class="checkbox-label" for="pg-lowercase">
                                                        Lowercase
                                                    </label>
                                                </div>
                                            <?php endif; ?>

                                            <?php if (isset($setting->number_visible) && $setting->number_visible == 1) : ?>
                                                <div class="checkbox">
                                                    <input class="checkbox-input password-filter" id="pg-numbers" type="checkbox" name="numbers" checked chset="0-9">
                                                    <label class="checkbox-label" for="pg-numbers">
                                                        Numbers
                                                    </label>
                                                </div>
                                            <?php endif; ?>

                                            <?php if (isset($setting->symbol_visible) && $setting->symbol_visible == 1) : ?>
                                                <div class="checkbox">
                                                    <input class="checkbox-input password-filter" id="pg-symbols" type="checkbox" name="symbols" checked chset="#">
                                                    <label class="checkbox-label" for="pg-symbols">
                                                        Symbols
                                                    </label>
                                                </div>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                    <div class="range-wrapper">
                                        <label class="range-label" for="password-length">
                                            Length
                                        </label>

                                        <div class="custom-range-inner">
                                            <input type="hidden" id="password-pin-hidden" value="<?php echo (isset($setting->minimum_slider)) ? $setting->minimum_slider : 1; ?>" />
                                            <input type="hidden" id="passphrase-hidden" value="2" />
                                            <div class="custom-range" id="password-range">
                                                <input type="range" class="custom" min="<?php echo (isset($setting->minimum_slider)) ? $setting->minimum_slider : 1; ?>" max="<?php echo (isset($setting->maximum_slider)) ? $setting->maximum_slider : 25; ?>" value="<?php echo (isset($setting->default_slider)) ? $setting->default_slider : 9; ?>" class="slider" id="myRange" style="width: 100%;" data-val="<?php echo (isset($setting->default_slider)) ? $setting->default_slider : 9; ?>">
                                            </div>
                                            <input value="<?php echo (isset($setting->default_slider)) ? $setting->default_slider : 9; ?>" name="length" class="range-number" id="password-length" type="number" data-val="<?php echo (isset($setting->default_slider)) ? $setting->default_slider : 9; ?>">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <div class="generated-wrapper-mob">
                            <button class="password-icon-generate refresh-password ga-reset-password" >
                                <i class="fas fa-redo-alt ga-reset-password"></i> <span>Generate </span>
                            </button>
                            <button class="copy-password copyPass ga-copy-pass affiliate-link" target-url="<?php echo $affiliatedUrl; ?>">
                                <i class="far fa-clone ga-copy-pass"></i> <span>Copy</span>
                            </button>
                        </div>
                    </div>

                </div>
            </div>
        </div>
</section>

<div class="modal-sec">
    <div class="modal fade" id="passwords" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog  modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">

                    <h3><span><i class="fas fa-check-circle"></i></span> Password Copied</h3>
                </div>
                <div class="modal-body icon">
                    <div class="inner outbound-link" data-category="username-resultspage" data-action="copyusername-popup" data-label="<?php echo $setting->copy_button_link; ?>">


                        <a href="<?php echo $setting->copy_button_link; ?>" target="_blank" class="affiliate-link" device="desktop">
                            <div class="text">
                                <div class="pass-img">
                                    <?php if ($setting->copy_image) : ?>
                                        <img src="<?php echo esc_url(plugins_url('images/' . $setting->copy_image, dirname(__FILE__))); ?>" alt="onepassword" />
                                    <?php endif; ?>
                                </div>

                                <h3><?php echo $setting->copy_title; ?> </h3>
                                <?php echo $setting->copy_description; ?>
                                <div class="pass-btn">
                                    <p><?php echo $setting->copy_button_text; ?></p>
                                </div>
                            </div>
                        </a>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="send" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog  modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="inner">
                        <div class="text">
                            <button type="button" style="display: none;" id="email-complete">Email Complete</button>

                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <li class="nav-item" role="presentation">
                                    <a class="nav-link active" id="send-email-tab" data-toggle="tab" href="#send-email" role="tab" aria-controls="sendmail" aria-selected="true">Send an Email</a>
                                </li>
                                <!-- <li class="nav-item" role="presentation">
                                        <a class="nav-link" id="send-mobile-tab" data-toggle="tab" href="#send-mobile" role="tab" aria-controls="sendmobile" aria-selected="false">Send to Mobile</a>
                                    </li> -->

                            </ul>
                            <div class="tab-content" id="myTabContent">
                                <div class="tab-pane fade show active" id="send-email" role="tabpanel" aria-labelledby="send-email-tab">
                                    <div class="email-sec">
                                        <form id="email-frm" method="post">
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="p_emails" placeholder="josh@passdowdhero.com" />
                                            </div>
                                            <div class="send-btn">
                                                <button class="send-button " type="submit" name="email_send" data-toggle="modal" data-target="#send">
                                                    <i class="far fa-paper-plane"></i> <span>Send</span>
                                                </button>
                                            </div>
                                            <?php if ($setting->is_opt == 1) : ?>
                                                <div class="email-opt">
                                                    <input type="checkbox" id="opt" class="form-control" name="opt_in" /> <label for="opt"><?php echo $setting->opt_message; ?></label>
                                                </div>
                                            <?php endif; ?>
                                        </form>
                                    </div>
                                    <div class="trial">
                                        <div class="box">
                                            <div class="brand">
                                                <?php if ($setting->send_image) : ?>
                                                    <img src="<?php echo esc_url(plugins_url('images/' . $setting->send_image, dirname(__FILE__))); ?>" alt="brand" />
                                                <?php endif; ?>
                                            </div>
                                            <h2><?php echo $setting->send_title; ?></h2>
                                            <?php echo $setting->send_description; ?>
                                            <a href="<?php echo $setting->send_button_link; ?>" class="affiliate-link"  device="desktop"><?php echo $setting->send_button; ?></a>
                                        </div>

                                    </div>
                                </div>

                            </div>
                            <div class="tab-pane fade" id="send-mobile" role="tabpanel" aria-labelledby="send-mobile-tab"></div>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

</div>

<style>
    div.tagsinput.error {
        border: 1px solid #f00;
    }
</style>
