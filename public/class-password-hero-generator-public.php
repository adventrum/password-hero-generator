<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @since      1.0.0
 *
 * @package    Password_Hero_Generator
 * @subpackage Password_Hero_Generator/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Password_Hero_Generator
 * @subpackage Password_Hero_Generator/public
 * @author     Sumit Sehgal <sumit@cybertrontechnologies.com>
 */
class Password_Hero_Generator_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Database Reference
	 * 
	 */
	private $wdb;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {
		global $wpdb;
		$this->plugin_name = $plugin_name;
		$this->version = $version;
		$this->wdb = $wpdb;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Plugin_Name_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Plugin_Name_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		

		wp_enqueue_style( $this->plugin_name.'-font-awesome', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.8.2/css/all.min.css', array(), $this->version, 'all' ); 

		wp_enqueue_style( $this->plugin_name.'-bootstrap', plugin_dir_url( __FILE__ ) . 'css/bootstrap.min.css', array(), $this->version, 'all' ); 

		wp_enqueue_style( $this->plugin_name.'-1', plugin_dir_url( __FILE__ ) . 'css/jquery.tagsinput-revisited.css', array(), $this->version, 'all' );
		wp_enqueue_style( $this->plugin_name.'-2', plugin_dir_url( __FILE__ ) . 'css/password-hero-generator-public.css', array(), $this->version, 'all' );
		wp_enqueue_style( $this->plugin_name.'-3', plugin_dir_url( __FILE__ ) . 'css/fonts.css', array(), $this->version, 'all' );
		
	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Plugin_Name_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Plugin_Name_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name.'-bootstrap', plugin_dir_url( __FILE__ ) . 'js/bootstrap.min.js', array( 'jquery' ), $this->version, true );

		wp_enqueue_script( $this->plugin_name.'-range-touch', plugin_dir_url( __FILE__ ) . 'js/range-touch.min.js', array( 'jquery' ), $this->version, true );

		wp_enqueue_script( $this->plugin_name.'-display-range-value', plugin_dir_url( __FILE__ ) . 'js/display-range-value.js', array( 'jquery' ), $this->version, true );	

		wp_enqueue_script( $this->plugin_name.'-1', plugin_dir_url( __FILE__ ) . 'js/jquery.tagsinput-revisited.js', array( 'jquery' ), $this->version, true );
		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/password-hero-generator-public.js', array( 'jquery' ), $this->version, true );

		wp_localize_script( $this->plugin_name, 'my_ajax_object',
            array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );

	}

	public function password_hero_box_copied($atts) {

		$copiedSetting = $this->wdb->get_row('SELECT * FROM '.$this->wdb->prefix.'password_hero_copy_settings ORDER BY id desc');

		include('views/password-hero-box-copied.php');

	}

	public function password_hero_box($atts) 
	{
		if($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['email_send'])) {

			$emails = explode(",", $_POST["p_emails"]);
			$from = "sumit@cybertrontechnologies.com";
			$message = "Message Body Test";
			if(!empty($emails)) {

				foreach($emails as $email) {
					$to = $email;
					$subject = "Password Generator Password";
					$headers = 'From: '. $from . "\r\n" .
					  'Reply-To: ' . $from . "\r\n";
					  
					//$sent = wp_mail($to, $subject, strip_tags($message), $headers);

				}

			}

		}
		
		$attributes = shortcode_atts( array(
			'title' => 'Password Generator1',
			'subtitle' => 'Create strong passwords that are completely random and impossible to guess.',
			'affiliated_url' => ''
		), $atts );

		$rules = $this->wdb->get_results('SELECT r.id, r.name, r.rule, r.platform_icon_class, r.fontawesome_class, r.status, rt.rule_id, rt.lbl_title, rt.rule_check FROM '. $this->wdb->prefix.'password_hero_rules r LEFT JOIN '.$this->wdb->prefix.'password_hero_platform_rule_titles rt ON r.id = rt.rule_id WHERE r.status = "Active" ORDER BY r.id asc ' );

		$setting = $this->wdb->get_row('SELECT * FROM '.$this->wdb->prefix.'password_hero_settings ORDER BY id desc');

		$overallRules = array();
		if(!empty($rules)) {
			foreach($rules as $rule) {
				if(!isset($overallRules[$rule->id])){
					$overallRules[$rule->id] = [
						'name' => $rule->name,
						'rule' => $rule->rule,
						'platform_icon_class' => $rule->platform_icon_class,
						'fontawesome_class' => $rule->fontawesome_class,
						'status' => $rule->status
					];
				}
				$overallRules[$rule->id]['titles'][] = [
					'lbl_title' => $rule->lbl_title,
					'rule_check' => $rule->rule_check
				];
			}
		}

		include('views/password-hero-box.php');
	}

	public function base_url(){
		return sprintf(
		  "%s://%s",
		  isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http',
		  $_SERVER['SERVER_NAME']
		);
	}

	public function email_action() {
		
		if($_SERVER["REQUEST_METHOD"] == "POST") {

			if(!empty($_POST["email"])) {
		
				$apiKey  = 'a3f8a4fd78561caab70aa6c4205c1a4c-us6';
				$listID  = 'f501040e65';
				$dataCenter = substr($apiKey,strpos($apiKey,'-')+1);
		
				$emails = $_POST["email"];
				$emailList = explode(",", $emails);
		
		
				if(!empty($emailList)) {
					foreach($emailList as $email) {
		
						if(isset($_POST["optin"]) && ($_POST["optin"] === true || $_POST["optin"] == "true" )  ){
							$memberID = md5(strtolower($email));
							$url = 'https://' . $dataCenter . '.api.mailchimp.com/3.0/lists/' . $listID . '/members/' . $memberID;
		
							$data = array(
								'apikey'        => $apiKey,
								'email_address' => $email,
								'status'        => 'subscribed',
								'tags' => [ 'PasswordHero-Stage' ] 
							);
		
							$json = json_encode($data);
		
							$ch = curl_init($url);
							curl_setopt($ch, CURLOPT_USERPWD, 'user:' . $apiKey);
							curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
							curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
							curl_setopt($ch, CURLOPT_TIMEOUT, 10);
							curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
							curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
							curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
							$result = curl_exec($ch);
							$httpCode = curl_getinfo( $ch, CURLINFO_HTTP_CODE );
							curl_close($ch);
						}
		
						$to = "$email";
						$subject = 'Your Password Generator Result';
		
						$headers = "From: josh.parreno@adventrum.com\r\n";
						$headers .= "Reply-To: josh.parreno@adventrum.com\r\n";
						$headers .= "MIME-Version: 1.0\r\n";
						$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
		
						$message = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
						<html xmlns="http://www.w3.org/1999/xhtml">
						
						<head>
							<meta name="viewport" content="width=device-width, initial-scale=1.0">
							<link rel="preconnect" href="https://fonts.googleapis.com">
							<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
							<link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,600;0,700;0,800;1,300;1,400;1,600;1,700;1,800&display=swap" rel="stylesheet">
							<title>Email</title>
							<style>
								body {
									padding: 0;
									margin: 0;
								}
								
								.logo {
									margin: 0 auto 40px;
									text-align: center;
									display: block;
								}
								
								.thanks-logo {
									margin-bottom: 28px;
								}
								
								.thanks-logo img {
									max-width: 100%;
								}
								
								.password-box {
									border: 1px solid rgba(0, 12, 51, 0.25);
									box-sizing: border-box;
									border-radius: 5px;
									padding: 25px 18px;
									height: 160px;
								}
								
								.password-box p {
									font-size: 24px;
									line-height: 29px;
									margin: 0px;
									color: #000C33;
									font-weight: bold;
									font-family: "Open Sans", sans-serif;
									opacity: 1;
								}
								
								.generate-pass {
									background: #000C33;
									border-radius: 5px;
									padding: 15px 20px;
									margin: 10px 0px 0px;
								}
								
								.generate-pass a {
									font-size: 16px;
									line-height: 19px;
									color: #fff;
									font-family: "Open Sans", sans-serif;
									text-align: center;
									display: block;
									text-decoration: none;
								}
								
								.brand img {
									max-width: 100%;
									margin-bottom: 8px;
								}
								
								.brand-thanks h2 {
									color: #a154ff;
									margin: 0px;
								}
								.brand h2 {
									font-family: "Open Sans", sans-serif;
									font-weight: bold;
									font-size: 24px;
									line-height: 29px;
									color: #000;
									margin: 0px;
								}
								
								.one-pass a {
									background: #50B9FF;
									border-radius: 5px;
									font-family: "Open Sans", sans-serif;
									font-size: 16px;
									line-height: 19px;
									color: #FFFFFF;
									display: block;
									padding: 15px 0px;
									width: 134px;
									text-align: center;
									text-decoration: none;
								}
								
								p {
									font-family: "Open Sans", sans-serif;
									font-size: 11px;
									color: #000C33;
									opacity: 0.5;
									margin: 0px;
								}
								
								p.foot a {
									color: #000C33;
									opacity: 1;
								}
								
								p.font {
									font-size: 14px;
								}
							</style>
						</head>
						
						<body style="background="#E5E5E5;">
							<table width="100%" align="center" bgcolor="#E5E5E5">
								<tr valign="top" align="center">
									<td align="center" valign="top">
										<table width="500" cellspacing="0" cellpadding="0" style="padding-top: 38px;padding-bottom: 60px;">
											<tbody>
												<tr class="head" valign="top">
													<td>
						
														<table width="100%" cellspacing="0" cellpadding="0" style="margin: 0 auto;">
															<tbody>
																<tr>
																	<td>
																		<a class="logo" style="margin: 0 auto 40px; text-align: center; display: block;" href="'.$this->base_url().'/"><img src="'.$this->base_url().'/wp-content/themes/passwordhero/assets/images/footer-logo.png" alt=""></a>
																	</td>
																</tr>
															</tbody>
														</table>
						
													</td>
												</tr>
												<tr>
													<td>
														<table  bgcolor="#fff" width=100%; style="padding: 28px 20px; background-color: #FFFFFF; border: 1px solid #A154FF; box-sizing: border-box; border-radius: 15px;">
															<tbody>
						
															<tr>
															<td>
																<table width="100%;" style="margin-bottom: 28px;">
																	<tbody>
																		<tr>
																			<td>
																				<div class="thanks-logo" style="margin:0px;">
																					<img style="max-width: 100%;" src="'.$this->base_url().'/shield.png" alt="">
																				</div>
																			</td>
																			<td> <div class="brand-thanks" style="color: #a154ff;">
																				<h2 style="font-size: 24px; line-height: 29px; margin: 0px; color: #a154ff; font-weight: bold; font-family: "Open Sans", sans-serif;">Thanks for using PasswordHero! <br> Here’s the pass you generated: </h2>
																			</div>
		
																			</td>
																		</tr>
																	</tbody>
																</table>
															</td>
														</tr>
																<tr>
																	<td>
																		<div class="password-box" style="border: 1px solid rgba(0, 12, 51, 0.25); box-sizing: border-box; border-radius: 5px;  padding: 25px 18px; height: 160px; -color:#fff;">
																			<p style="font-size: 24px; line-height: 29px; margin: 0px;color: #000C33;font-weight: bold; font-family: "Open Sans", sans-serif; opacity: 1;">'.$_POST["password"].'</p>
																		</div>
																		<div class="generate-pass" style="background-color: #000C33; border-radius: 5px;  padding: 15px 20px; margin: 10px 0px 0px;">
																			<a style=" font-size: 16px; line-height: 19px; color: #fff; font-family: "Open Sans", sans-serif; text-align: center; display: block;text-decoration: none;" href="'.$this->base_url().'">Click here to generate again</a>
																		</div>
																	</td>
																</tr>
															</tbody>
														</table>
													</td>
												</tr>
												<tr>
													<td>
														<table width=100%; style="margin-top: 10px; padding: 28px 20px; background-color: #FFFFFF; border: 1px solid rgba(0, 12, 51, 0.25); box-sizing: border-box; border-radius: 15px;">
															<tbody>
																<tr>
																	<td>
																		<div class="brand">
																			<img style="max-width: 100%; margin-bottom: 8px;"  src="'.$this->base_url().'/brand.png" alt="brand">
																		</div>
																	</td>
																</tr>
																<tr>
																	<td>
																		<div class="brand">
																			<h2 style=" font-family: "Open Sans", sans-serif; font-weight: bold;  font-size: 24px;   line-height: 29px;  color: #000000; margin: 0px;">Store all your passwords securely in one place.</h2>
																		</div>
																	</td>
																	<td>
																		<div class="one-pass">
																			<a style="background-color:#50B9FF; border-radius: 5px; font-family: "Open Sans", sans-serif; font-size: 16px; line-height: 19px;color: #FFFFFF; display: block; padding: 15px 0px; width: 134px; text-align: center;text-decoration: none;" href="https://www.tkqlhce.com/fb106biroiq5766AC8C7B57AACACFD">Try it Free</a>
																		</div>
																	</td>
																</tr>
						
															</tbody>
														</table>
						
													</td>
												</tr>
						
												<tr>
													<td>
						
														<table width=100%; align="center" style="text-align: center; margin-top: 40px;">
															<tbody>
																<tr>
																	<td>
																		<p style=" font-family: "Open Sans", sans-serif; font-size: 14px; color: #000C33;  opacity: 0.5; margin: 0px;" class="font">© 2021 PasswordHero. All rights reserved.</p>
																	</td>
																</tr>
																<tr>
																	<td>
																		<p style=" font-family: "Open Sans", sans-serif;font-size: 11px; color: #000C33;  opacity: 0.5; margin: 0px;" class="foot">This email was sent to josh.parreno@adventrum.com View our <a style="color: #000C33; opacity: 1;"  href="'.$this->base_url().'/privacy-policy"> Privacy Policy</a>.<br> If you no longer wish to receive these emails you can <a href="'.$this->base_url().'/unsubscribe.php?email='.$email.'" style="color: #000C33; opacity: 1;"> unsubscribe </a> at any time.</p>
																	</td>
																</tr>
						
															</tbody>
														</table>
						
													</td>
												</tr>
											</tbody>
										</table>
									</td>
								</tr>
							</table>
						</body>
						
						</html>';
		
							
			@mail( $to, $subject, $message, $headers);
					}
				}
		
				echo "OK";
				exit;
			}
		}
		echo "FAILED";
		exit;

	}

}