<?php

/**
* Plugin Name: Password Hero Generator
* Description: It generates Random and Secure Passwords
* Version: 1.0.0
* Author: Sumit Sehgal
* Author URI: http://cybertrontechnologies.com
*/

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}
/**
 * Plugin Current Version
 */
define( 'PASSWORD_HERO_GENERATOR_VERSION', '1.0.0' );


/**
 * On Plugin Activation
 */
function activate_password_hero_generator() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-password-hero-generator-activator.php';
	Password_Hero_Generator_Activator::activate();
}

/**
 * On Plugin Deactivation
 */
function deactivate_password_hero_generator() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-password-hero-generator-deactivator.php';
	Password_Hero_Generator_Deactivator::deactivate();
}

/**
 * On Plugin Uninstall
 */
function uninstall_password_hero_generator() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-password-hero-generator-uninstall.php';
	Password_Hero_Generator_Uninstall::uninstall();
}

register_activation_hook( __FILE__, 'activate_password_hero_generator' );
register_deactivation_hook( __FILE__, 'deactivate_password_hero_generator' );
register_uninstall_hook( __FILE__, 'uninstall_password_hero_generator' );

/**
 * Core Plugin Code
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-password-hero-generator.php';

/**
 * Begins execution of the plugin.
 *
 *
 * @since    1.0.0
 */
function run_password_hero_generator() {

	$plugin = new Password_Hero_Generator();
	$plugin->run();

}
run_password_hero_generator();