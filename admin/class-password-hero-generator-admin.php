<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @since      1.0.0
 *
 * @package    Password_Hero_Generator
 * @subpackage Password_Hero_Generator/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Password_Hero_Generator
 * @subpackage Password_Hero_Generator/admin
 * @author     Sumit Sehgal <sumit@cybertrontechnologies.com>
 */
class Password_Hero_Generator_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Database Reference
	 * 
	 */
	private $wdb;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {
		global $wpdb;
		$this->plugin_name = $plugin_name;
		$this->version = $version;
		$this->wdb = $wpdb;
		

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/password-hero-generator-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {
		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/password-hero-generator-admin.js', array( 'jquery' ), $this->version, false );

	}

	/**
	 * Admin Menu
	 * 
	 * @since 1.0.0
	 */
	public function add_menus() {
		add_menu_page('Password Hero Admin Page', 'Password Hero', 'manage_options', 'password-hero', array($this, "index_action"));

		add_submenu_page('password-hero', 'Add New Rule', 'Add New Rule', 'manage_options', 'password-hero-add-rule', array($this, "add_rule_action"));

		add_submenu_page('password-hero', 'Settings', 'Settings', 'manage_options', 'password-hero-settings', array($this, "settings_action"));

		add_submenu_page('password-hero', 'Copied Settings', 'Copied Settings', 'manage_options', 'password-hero-copy-settings', array($this, "copy_settings_action"));

		add_submenu_page(null, 'Edit Rule', 'Edit Rule', 'manage_options', 'password-hero-edit-rule', array($this, "edit_rule_action"));

		add_submenu_page(null, 'Delete Rule', 'Delete Rule', 'manage_options', 'password-hero-delete-rule', array($this, "delete_rule_action"));
		
	}

	public function index_action() {
		$rules = $this->wdb->get_results('SELECT * FROM '. $this->wdb->prefix.'password_hero_rules' );

		$path = 'admin.php?page=password-hero-add-rule';
		$url = admin_url($path);
		include_once( 'views/index.php' );

	}

	public function copy_settings_action() {

		if($_SERVER["REQUEST_METHOD"] == "POST") {
			$fields = array(
				'title' => stripslashes($_POST['title']),
				'highlight_title' => stripslashes($_POST['highlight_title']),
				'tagline' => stripslashes($_POST['tagline']),
				'subtext' => stripslashes($_POST['subtext']),
				'agreement_text' => stripslashes($_POST['agreement_text']),
			);



			$result = $this->wdb->update($this->wdb->prefix.'password_hero_copy_settings', $fields, array('id'=> $_POST['id']));


		}

		$copySetting = $this->wdb->get_row('SELECT * FROM '.$this->wdb->prefix.'password_hero_copy_settings ORDER BY id desc');

		include_once( 'views/copy-settings.php' );
		
	}

	public function settings_action() {

		if($_SERVER["REQUEST_METHOD"] == "POST") {

			$uploadDir = ABSPATH.'wp-content/plugins/password-hero-generator/public/images/';
			$fileNames = [];
			foreach($_FILES as $key=>$file) {
				if($file["error"] == 0) {
					$fileInfo = explode(".",$file["name"]);
					$extension = end($fileInfo);
					$fileNames[$key] = time().".".$extension;
					move_uploaded_file($file['tmp_name'], $uploadDir.$fileNames[$key]);
				}else {
					$fileNames[$key] = false;
				}
			}
			

			$fields = array(
				'minimum_slider'=>esc_attr($_POST['minimum_slider']), 
				'maximum_slider'=>esc_attr($_POST['maximum_slider']),
				'default_slider' => esc_attr($_POST['default_slider']),
				'uppercase_visible' => (isset($_POST['uppercase_visible']) && $_POST['uppercase_visible'] == "on") ? 1 : 0,
				'lowercase_visible' => (isset($_POST['lowercase_visible']) && $_POST['lowercase_visible'] == "on") ? 1 : 0,
				'number_visible' => (isset($_POST['number_visible']) && $_POST['number_visible'] == "on") ? 1 : 0,
				'symbol_visible' => ( isset($_POST['symbol_visible']) && $_POST['symbol_visible'] == "on") ? 1 : 0,
				'platform_visible' => (isset($_POST['platform_visible']) && $_POST['platform_visible'] == "on") ? 1 : 0,
				'copy_title' => stripslashes($_POST['copy_title']),
				'copy_description' => stripslashes($_POST['copy_description']),
				'copy_button_text' => stripslashes($_POST['copy_button_text']),
				'copy_button_link' => esc_attr($_POST['copy_button_link']),
				'send_title' => stripslashes($_POST['send_title']),
				'send_description' => stripslashes($_POST['send_description']),
				'send_button' => esc_attr($_POST['send_button']),
				'send_button_link' => esc_attr($_POST['send_button_link']),
				'is_opt' => (isset($_POST['is_opt']) && $_POST['is_opt'] == "on") ? 1 : 0,
				'opt_message' => stripslashes($_POST['opt_message']),
				'password_hero_redirect' => stripslashes($_POST['password_hero_redirect']),
				'password_hero_redirect_type' => stripslashes($_POST['password_hero_redirect_type'])
			);

			if($fileNames['copy_image'] != false) {
				$fields['copy_image'] = $fileNames['copy_image'];
			}

			if($fileNames['send_image'] != false) {
				$fields['send_image'] = $fileNames['send_image'];
			}


			$result = $this->wdb->update($this->wdb->prefix.'password_hero_settings', $fields, array('id'=> $_POST['id']));


		}



		$setting = $this->wdb->get_row('SELECT * FROM '.$this->wdb->prefix.'password_hero_settings ORDER BY id desc');

		include_once( 'views/settings.php' );
	}

	public function add_rule_action() {
		if($_SERVER["REQUEST_METHOD"] == "POST") {

			$exists = $this->wdb->get_row('SELECT * FROM '.$this->wdb->prefix.'password_hero_rules WHERE name = "'.$_POST['name'].'"');
			if($exists) {
				$rule = new stdClass();
				$rule->name = $_POST["name"];
				$rule->rule = $_POST["rule"];
				$error["name"][] = "Platform Already Exists.";
				include_once( 'views/add-rule.php' );
			}else {

				$result = $this->wdb->insert($this->wdb->prefix.'password_hero_rules', [
					'name' => $_POST['name'],
					'rule' => $_POST['regex'],
					'platform_icon_class' => $_POST['platform_icon_class'],
					'fontawesome_class' => $_POST['fontawesome_class'],
					'status' => $_POST['status']
				]);

				$ruleId = $this->wdb->insert_id;
				
				if(!empty($_POST['rule_title'])) {
					foreach($_POST['rule_title'] as $rtitle) {
						$this->wdb->insert($this->wdb->prefix.'password_hero_platform_rule_titles', [
							'lbl_title' => $rtitle['lbl_title'],
							'rule_check' => $rtitle['rule_check'],
							'rule_id' => $ruleId
						]);
					}
				}



				$path = 'admin.php?page=password-hero';
				$url = admin_url($path);
				include_once('views/redirect.php');
				exit;
			}
		}else {
			include_once( 'views/add-rule.php' );
		}
	}

	public function edit_rule_action() {

		$id = isset($_GET['id']) ? $_GET['id'] : null;
		if(!$id) {
			echo "There is no such rule Exists. Please Go Back and Try Again.";
			exit;
		}
		$sql = 'SELECT * FROM '. $this->wdb->prefix.'password_hero_rules WHERE id = '.$id;
		$rule = $this->wdb->get_row($sql);
		if(!$rule) {
			echo "There is no such rule Exists. Please Go Back and Try Again.";
			exit;
		}

		$sql = 'SELECT * FROM '.$this->wdb->prefix.'password_hero_platform_rule_titles WHERE rule_id='.$id;
		$ruletitles = $this->wdb->get_results($sql);

		if($_SERVER["REQUEST_METHOD"] == "POST") {
			$exists = $this->wdb->get_row('SELECT * FROM '.$this->wdb->prefix.'password_hero_rules WHERE name = "'.$_POST['name'].'" AND id != '.$id);
			if($exists) {
				$rule = new stdClass();
				$rule->name = $_POST["name"];
				$rule->rule = $_POST["regex"];
				$error["name"][] = "Platform Already Exists.";
				include_once( 'views/edit-rule.php' );
			}else {
				$result = $this->wdb->update($this->wdb->prefix.'password_hero_rules', array(
					'name'=>$_POST['name'], 
					'rule'=>$_POST['regex'],
					'platform_icon_class' => $_POST['platform_icon_class'],
					'fontawesome_class' => $_POST['fontawesome_class'],
					'status' => $_POST['status']
				), array('id'=>$id));

				$this->wdb->delete( $this->wdb->prefix.'password_hero_platform_rule_titles', array( 'rule_id' => $id ) );

				if(!empty($_POST['rule_title'])) {
					foreach($_POST['rule_title'] as $rtitle) {
						$this->wdb->insert($this->wdb->prefix.'password_hero_platform_rule_titles', [
							'lbl_title' => $rtitle['lbl_title'],
							'rule_check' => $rtitle['rule_check'],
							'rule_id' => $id
						]);
					}
				}

				$path = 'admin.php?page=password-hero';
				$url = admin_url($path);
				include_once('views/redirect.php');
				exit;

			}

		}else {
			include_once( 'views/edit-rule.php' );
		}

	}

	public function delete_rule_action() {

		$id = isset($_GET['id']) ? $_GET['id'] : null;
		if(!$id) {
			echo "There is no such rule Exists. Please Go Back and Try Again.";
			exit;
		}

		$sql = 'SELECT * FROM '. $this->wdb->prefix.'password_hero_rules WHERE id = '.$id;
		$rule = $this->wdb->get_row($sql);
		if(!$rule) {
			echo "There is no such rule Exists. Please Go Back and Try Again.";
			exit;
		}

		$this->wdb->delete( $this->wdb->prefix.'password_hero_rules', array( 'id' => $id ) );

		$path = 'admin.php?page=password-hero';
		$url = admin_url($path);
		include_once('views/redirect.php');
		exit;
	}

} ?>