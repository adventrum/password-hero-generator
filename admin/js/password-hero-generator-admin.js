
jQuery("document").ready(function(){
    jQuery('#createrule').on("submit", function(){
        var isValid = true;
        jQuery(".required").each(function() {
            if(jQuery(this).val() == "") {
                isValid = false;
                jQuery(this).parents("tr").first().addClass('form-invalid');
            }else {
                jQuery(this).parents("tr").first().removeClass('form-invalid');
            }
        });

        return isValid;
    });

    jQuery(".password-hero-rule-delete").on("click", function() {

        return confirm("Do you want to delete the rule?");

    });

    var idx = 1;
    if(jQuery("#latest-count").length > 0){
        idx = parseInt(jQuery("#latest-count").val());
        console.log(idx);
    }
    jQuery(".add-title").on("click", function(){
        
        var html = '<tr class="form-field form-required" style="text-align: center;"> <td> <select name="rule_title['+idx+'][rule_check]"> <option value="length">Length</option> <option value="uppercase">Uppercase</option> <option value="lowercase">Lowercase</option> <option value="number">Number</option> <option value="symbol">Symbol</option> <option value="other">Other</option> </select> </td><td> <input type="text" name="rule_title['+idx+'][lbl_title]"/> </td><td> <a href="javascript:void(0);" class="delete-title">X</a> </td></tr>';

        jQuery(this).parents('tbody').append(html);
        idx++;

        return false;

    });

    jQuery('table').on("click", '.delete-title', function() {
        jQuery(this).parents('tr').first().remove();
        return false;
    });


})