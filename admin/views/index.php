<div class="wrap">
    <h1 class="wp-heading-inline"><?php echo esc_html( get_admin_page_title() ); ?></h1>
    <a href="<?php echo $url; ?>" class="page-title-action">Add New Rule</a>
    <hr class="wp-header-end">
    <ul class="subsubsub"></ul>
    <form method="get">
        <h2 class="screen-reader-text">Users list</h2>
        <table class="wp-list-table widefat fixed striped table-view-list users">
            <thead>
                <tr>
                    <th class="manage-column">ID</th>
                    <th class="manage-column">Platform Name</th>
                    <th class="manage-column">Regex</th>
                </tr>
            </thead>

            <tbody>
                <?php if(!empty($rules)): ?>
                    <?php foreach($rules as $rule): ?>
                <tr>
                    <td ><?php echo $rule->id; ?></td>
                    <td >
                        <strong><?php echo $rule->name; ?></strong>
                        <div class="row-actions">
                            <?php $editPath = 'admin.php?page=password-hero-edit-rule&id='.$rule->id;
		                          $editUrl = admin_url($editPath);
                            ?>
                            <span class="edit">
                                <a href="<?php echo $editUrl; ?>">Edit</a> | 
                            </span>
                            <span class="view">
                            <?php $deletePath = 'admin.php?page=password-hero-delete-rule&id='.$rule->id;
		                          $deleteUrl = admin_url($deletePath);
                            ?>
                                <a href="<?php echo $deleteUrl; ?>" aria-label="Delete" class="password-hero-rule-delete">Delete</a>
                            </span>
                        </div>
                    </td>
                    <td ><?php echo $rule->rule; ?></td>
                </tr>
                        <?php endforeach; ?>
                <?php endif; ?>
            </tbody>
        
        </table>
    </form>
    
 
</div><!-- .wrap -->