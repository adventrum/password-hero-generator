<div class="wrap">
    <h1 class="wp-heading-inline"><?php echo esc_html(get_admin_page_title()); ?></h1>
    <hr class="wp-header-end">
    <ul class="subsubsub"></ul>

    <form method="post" enctype="multipart/form-data">
        <input type="hidden" name="id" value="<?php echo $copySetting->id; ?>" />

        <table class="form-table">
            <tbody>
                <tr class="form-field form-required ">
                    <th scope="row"><strong style="text-decoration: underline;">Copied Widget Settings</strong></th>
                    <td></td>
                </tr>

                <tr class="form-field form-required ">
                    <th scope="row">
                        <label for="title">Title <span class="description"></span></label>
                    </th>
                    <td>
                        <input name="title" type="text" id="title" value="<?php echo isset($copySetting) ? $copySetting->title : ''; ?>" aria-required="true" autocapitalize="none" autocorrect="off">
                    </td>
                </tr>

                <tr class="form-field form-required ">
                    <th scope="row">
                        <label for="highlight_title">Highlight Title <span class="description"></span></label>
                    </th>
                    <td>
                        <input name="highlight_title" type="text" id="highlight_title" value="<?php echo isset($copySetting) ? $copySetting->highlight_title : ''; ?>" aria-required="true" autocapitalize="none" autocorrect="off">
                    </td>
                </tr>

                <tr class="form-field form-required ">
                    <th scope="row">
                        <label for="tagling">Tagline <span class="description"></span></label>
                    </th>
                    <td>
                        <textarea name="tagline" id="tagline" ><?php echo (isset($copySetting) && !empty($copySetting->tagline) ) ? $copySetting->tagline : ''; ?></textarea>
                    </td>
                </tr>

                <tr class="form-field form-required ">
                    <th scope="row">
                        <label for="subtext">Subtext <span class="description"></span></label>
                    </th>
                    <td>
                        <textarea name="subtext" id="subtext" ><?php echo isset($copySetting) ? $copySetting->subtext : ''; ?></textarea>
                    </td>
                </tr>

                <tr class="form-field form-required ">
                    <th scope="row">
                        <label for="agreement_text">Agreement Text <span class="description"></span></label>
                    </th>
                    <td>
                        <textarea name="agreement_text" id="agreement_text" ><?php echo isset($copySetting) ? $copySetting->agreement_text : ''; ?></textarea>
                    </td>
                </tr>



            </tbody>
        </table>

        <?php
        $btnText = "Update Settings";
        ?>
        <p class="submit"><input type="submit" class="button button-primary" value="<?php echo $btnText; ?>"></p>


    </form>





</div>