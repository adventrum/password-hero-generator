<div class="wrap">
    <h1 class="wp-heading-inline"><?php echo esc_html(get_admin_page_title()); ?></h1>
    <hr class="wp-header-end">
    <ul class="subsubsub"></ul>

    <form method="post" enctype="multipart/form-data">
        <input type="hidden" name="id" value="<?php echo $setting->id; ?>" />
        <table class="form-table">
            <tbody>
                <tr class="form-field form-required ">
                    <th scope="row"><strong style="text-decoration: underline;">Range Value Settings</strong></th>
                    <td></td>
                </tr>
                <tr class="form-field form-required ">
                    <th scope="row">
                        <label for="minimum_slider">Minimum Range Value <span class="description"></span></label>
                    </th>
                    <td>
                        <input name="minimum_slider" type="number" id="minimum_slider" value="<?php echo isset($setting) ? $setting->minimum_slider : 0; ?>" aria-required="true" autocapitalize="none" autocorrect="off" maxlength="60">
                    </td>
                </tr>

                <tr class="form-field form-required ">
                    <th scope="row">
                        <label for="maximum_slider">Maximum Range Value <span class="description"></span></label>
                    </th>
                    <td>
                        <input name="maximum_slider" type="number" id="maximum_slider" value="<?php echo isset($setting) ? $setting->maximum_slider : 0; ?>" aria-required="true" autocapitalize="none" autocorrect="off" maxlength="60">
                    </td>

                </tr>

                <tr class="form-field form-required ">
                    <th scope="row">
                        <label for="default_slider">Default Value <span class="description"></span></label>
                    </th>
                    <td>
                        <input name="default_slider" type="number" id="default_slider" value="<?php echo isset($setting) ? $setting->default_slider : 0; ?>" aria-required="true" autocapitalize="none" autocorrect="off" maxlength="60">
                    </td>
                </tr>
                
                <tr class="form-field form-required ">
                    <th scope="row"><strong style="text-decoration: underline;">Filter Settings</strong></th>
                    <td></td>
                </tr>

                <tr class="form-field form-required ">
                    <th scope="row">
                        <label for="uppercase_visible">Uppercase Visibility <span class="description"></span></label>
                    </th>
                    <td>
                        <input name="uppercase_visible" type="checkbox" id="uppercase_visible" <?php echo (isset($setting->uppercase_visible) && $setting->uppercase_visible == 1) ? "checked" : "";  ?> >
                    </td>
                </tr>

                <tr class="form-field form-required ">
                    <th scope="row">
                        <label for="lowercase_visible">Lowercase Visibility <span class="description"></span></label>
                    </th>
                    <td>
                        <input name="lowercase_visible" type="checkbox" id="lowercase_visible" <?php echo (isset($setting->lowercase_visible) && $setting->lowercase_visible == 1) ? "checked" : "";  ?> >
                    </td>
                </tr>

                <tr class="form-field form-required ">
                    <th scope="row">
                        <label for="number_visible">Number Visibility <span class="description"></span></label>
                    </th>
                    <td>
                        <input name="number_visible" type="checkbox" id="number_visible" <?php echo (isset($setting->number_visible) && $setting->number_visible == 1) ? "checked" : "";  ?> >
                    </td>
                </tr>

                <tr class="form-field form-required ">
                    <th scope="row">
                        <label for="symbol_visible">Symbol Visibility <span class="description"></span></label>
                    </th>
                    <td>
                        <input name="symbol_visible" type="checkbox" id="symbol_visible" <?php echo (isset($setting->symbol_visible) && $setting->symbol_visible == 1) ? "checked" : "";  ?> >
                    </td>
                </tr>

                <tr class="form-field form-required ">
                    <th scope="row">
                        <label for="platform_visible">Platform Visibility <span class="description"></span></label>
                    </th>
                    <td>
                        <input name="platform_visible" type="checkbox" id="platform_visible" <?php echo (isset($setting->platform_visible) && $setting->platform_visible == 1) ? "checked" : "";  ?> >
                    </td>
                </tr>

                <tr class="form-field form-required ">
                    <th scope="row"><strong style="text-decoration: underline;">Copy Popup Settings</strong></th>
                    <td></td>
                </tr>

                <tr class="form-field form-required ">
                    <th scope="row">
                        <label for="copy_title">Title<span class="description"></span></label>
                    </th>
                    <td>
                        <textarea name="copy_title" id="copy_title"><?php echo isset($setting) ? $setting->copy_title : ''; ?></textarea>
                    </td>
                </tr>

                <tr class="form-field form-required ">
                    <th scope="row">
                        <label for="copy_description">Description<span class="description"></span></label>
                    </th>
                    <td>
                        <textarea name="copy_description" id="copy_description" rows="8"><?php echo isset($setting) ? $setting->copy_description : ''; ?></textarea>
                    </td>
                </tr>


                <tr class="form-field form-required ">
                    <th scope="row">
                        <label for="copy_image">Image<span class="description"></span></label>
                    </th>
                    <td>
                        <input name="copy_image" type="file" id="copy_image"  accept="image/*">
                        <?php echo isset($setting) ? $setting->copy_image : ''; ?>
                    </td>
                </tr>

                <tr class="form-field form-required ">
                    <th scope="row">
                        <label for="copy_button_text">Button Text<span class="description"></span></label>
                    </th>
                    <td>
                        <input name="copy_button_text" type="text" id="copy_button_text" value="<?php echo isset($setting) ? $setting->copy_button_text : ''; ?>" aria-required="true" autocapitalize="none" autocorrect="off" maxlength="60">
                    </td>
                </tr>

                <tr class="form-field form-required ">
                    <th scope="row">
                        <label for="copy_button_link">Button Link<span class="description"></span></label>
                    </th>
                    <td>
                        <input name="copy_button_link" type="text" id="copy_button_link" value="<?php echo isset($setting) ? $setting->copy_button_link : ''; ?>" aria-required="true" autocapitalize="none" autocorrect="off" maxlength="60">
                    </td>
                </tr>

                <!--  Send Pop Up -->
                <tr class="form-field form-required ">
                    <th scope="row"><strong style="text-decoration: underline;">Send Popup Settings</strong></th>
                    <td></td>
                </tr>

                <tr class="form-field form-required ">
                    <th scope="row">
                        <label for="send_title">Title<span class="description"></span></label>
                    </th>
                    <td>
                        <textarea name="send_title" id="send_title"  ><?php echo isset($setting) ? $setting->send_title : ''; ?></textarea>
                        
                    </td>
                </tr>

                <tr class="form-field form-required ">
                    <th scope="row">
                        <label for="send_description">Description<span class="description"></span></label>
                    </th>
                    <td>
                        <textarea name="send_description" id="send_description"  rows="8"><?php echo isset($setting) ? $setting->send_description : ''; ?></textarea>
                    </td>
                </tr>


                <tr class="form-field form-required ">
                    <th scope="row">
                        <label for="send_image">Image<span class="description"></span></label>
                    </th>
                    <td>
                        <input name="send_image" type="file" id="send_image" accept="image/*" >
                        <?php echo isset($setting) ? $setting->send_image : ''; ?>
                    </td>
                </tr>

                <tr class="form-field ">
                    <th scope="row">
                        <label for="is_opt">Opt Visibility <span class=""></span></label>
                    </th>
                    <td>
                        <input name="is_opt" type="checkbox" id="is_opt" <?php echo (isset($setting->is_opt) && $setting->is_opt == 1) ? "checked" : "";  ?> >
                    </td>
                </tr>

                <tr class="form-field">
                    <th scope="row">
                        <label for="opt_message">Opt Message<span class="description"></span></label>
                    </th>
                    <td>
                        <textarea name="opt_message" id="opt_message"  rows="8"><?php echo isset($setting) ? $setting->opt_message : ''; ?></textarea>
                    </td>
                </tr>

                <tr class="form-field form-required ">
                    <th scope="row">
                        <label for="send_button_text">Button Text<span class="description"></span></label>
                    </th>
                    <td>
                        <input name="send_button" type="text" id="send_button" value="<?php echo isset($setting) ? $setting->send_button : ''; ?>" aria-required="true" autocapitalize="none" autocorrect="off" maxlength="60">
                    </td>
                </tr>

                <tr class="form-field form-required ">
                    <th scope="row">
                        <label for="send_button_link">Button Link<span class="description"></span></label>
                    </th>
                    <td>
                        <input name="send_button_link" type="text" id="send_button_link" value="<?php echo isset($setting) ? $setting->send_button_link : ''; ?>" aria-required="true" autocapitalize="none" autocorrect="off" maxlength="60">
                    </td>
                </tr>

                <!-- Redirection Setting -->
                <tr class="form-field form-required ">
                    <th scope="row"><strong style="text-decoration: underline;">Redirect Settings</strong></th>
                    <td></td>
                </tr>

                <tr class="form-field form-required ">
                    <th scope="row">
                        <label for="password_hero_redirect">Redirect Link<span class="description"></span></label>
                    </th>
                    <td>
                        <input name="password_hero_redirect" type="text" id="password_hero_redirect" value="<?php echo isset($setting) ? $setting->password_hero_redirect : ''; ?>" aria-required="true" autocapitalize="none" autocorrect="off" maxlength="60">
                    </td>
                </tr>

                <tr class="form-field form-required ">
                    <th scope="row">
                        <label for="password_hero_redirect_type">Link Open In<span class="description"></span></label>
                    </th>
                    <td>
                        <select name="password_hero_redirect_type">
                            <option value="same-window" <?php if( isset($setting) && $setting->password_hero_redirect_type == "same-window" ) { echo "selected"; } ?>  >Same Window</option>
                            <option value="new-window" <?php if( isset($setting) && $setting->password_hero_redirect_type == "new-window" ) { echo "selected"; } ?>  >New Window</option>
                        </select>
                    </td>
                </tr>

                

                
                

            </tbody>
        </table>
        <?php
        $btnText = "Update Settings";
        ?>
        <p class="submit"><input type="submit" class="button button-primary" value="<?php echo $btnText; ?>"></p>

    </form>

</div>