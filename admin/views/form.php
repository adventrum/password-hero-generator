<form method="post" id="createrule">
    <table class="form-table">
        <tbody>
            <tr class="form-field form-required <?php echo (isset($error) && isset($error['name'])) ? 'form-invalid' : '' ?> ">
                <th scope="row">
                    <label for="name">Platform Name <span class="description">(required)</span></label>
                    <?php if (isset($error) && isset($error["name"])) : ?>

                        <?php foreach ($error['name'] as $single) : ?>
                            <br />
                            <span class="custom-rule-error"><?php echo $single; ?></span>
                        <?php endforeach; ?>

                    <?php endif; ?>
                </th>
                <td>
                    <input name="name" type="text" id="name" value="<?php echo isset($rule) ? $rule->name : ''; ?>" aria-required="true" autocapitalize="none" autocorrect="off" maxlength="60" class="required">
                </td>

            </tr>

            <tr class="form-field">
                <th scope="row"><label for="regex">Regex </label></th>
                <td><input name="regex" type="text" id="regex" value="<?php echo isset($rule) ? $rule->rule : ''; ?>"></td>
            </tr>

            <tr class="form-field form-required">
                <th scope="row">
                    <label for="platform_icon_class">Platform Icon Class</label>
                </th>
                <td>
                    <input name="platform_icon_class" type="text" id="platform_icon_class" value="<?php echo isset($rule) ? $rule->platform_icon_class : ''; ?>" aria-required="true" autocapitalize="none" autocorrect="off" maxlength="60">
                </td>

            </tr>

            <tr class="form-field form-required">
                <th scope="row">
                    <label for="fontawesome_class">Fontawesome Class</label>
                </th>
                <td>
                    <input name="fontawesome_class" type="text" id="fontawesome_class" value="<?php echo isset($rule) ? $rule->fontawesome_class : ''; ?>" aria-required="true" autocapitalize="none" autocorrect="off" maxlength="60">
                </td>
            </tr>

            <tr class="form-field form-required">
                <th scope="row">
                    <label for="status">Status</label>
                </th>
                <td>
                    <select name="status">
                        <option value="Active">Active</option>
                        <option value="Inactive">Inactive</option>
                    </select>
                </td>
            </tr>

            <tr class="form-field form-required" style="text-align: center; text-decoration: underline;">
                <th style="text-align: center;">
                    <label for="">Rule Title Type</label>
                </th>
                <th style="text-align: center;">
                    <label for="">Rule Title Label</label>
                </th>
                <th style="text-align: center;">
                    <a href="javascript:void(0);" class="btn btn-success add-title">Add</a>
                </th>

            </tr>

            <?php if (!empty($ruletitles)) : ?>
                <?php foreach ($ruletitles as $key => $rt) : ?>

                    <tr class="form-field form-required" style="text-align: center;">
                        <td>
                            <select name="rule_title[<?php echo $key; ?>][rule_check]">
                                <option value="length" <?php echo ( $rt->rule_check == "length") ? 'selected' : ''; ?> >Length</option>
                                <option value="uppercase" <?php echo ( $rt->rule_check == "uppercase") ? 'selected' : ''; ?>>Uppercase</option>
                                <option value="lowercase" <?php echo ( $rt->rule_check == "lowercase") ? 'selected' : ''; ?>>Lowercase</option>
                                <option value="number" <?php echo ( $rt->rule_check == "number") ? 'selected' : ''; ?>>Number</option>
                                <option value="symbol" <?php echo ( $rt->rule_check == "symbol") ? 'selected' : ''; ?>>Symbol</option>
                                <option value="other" <?php echo ( $rt->rule_check == "other") ? 'selected' : ''; ?>>Other</option>
                            </select>
                        </td>

                        <td>
                            <input type="text" name="rule_title[<?php echo $key; ?>][lbl_title]" value="<?php echo $rt->lbl_title; ?>" />
                        </td>
                        <td>
                            <a href="javascript:void(0);" class="delete-title">X</a>
                        </td>
                    
                    </tr>

                <?php endforeach; ?>
                <input type="hidden" id="latest-count" value="<?php echo count($ruletitles);?>" />"
            <?php endif; ?>





        </tbody>
    </table>
    <?php
    $btnText = (isset($id)) ? "Update Rule" : "Add New Rule";
    ?>
    <p class="submit"><input type="submit" name="createrule" id="createrulebtn" class="button button-primary" value="<?php echo $btnText; ?>"></p>

</form>